import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Random;


public class SnakesAndLaddersGame {

    // initiating the JFrame and JPanel
    static JFrame f = new JFrame();
    static JLayeredPane gamePanel = new JLayeredPane();

    static JButton p1Position = new JButton();
    static JButton p2Position = new JButton();

    // variables used for game logic
    static boolean gameStarted = false;
    static int turnCounter = 0;
    static int p1Counter = 1;
    static int p2Counter = 1;
    static int place = 37;

    SnakesAndLaddersGame() {

        addGUI();
        createBoard();

    }



    // creating the GUI
    public static void addGUI() {
        // setting the size of the JFrame and making it visible
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        f.setSize(1600, 900);
        f.setLayout(null);
        f.setVisible(true);

        // setting the size of the JPanel
        gamePanel.setBounds(0,0, 1600,900);
        gamePanel.setLayout(null);
        gamePanel.setVisible(true);

        f.add(gamePanel);

        // ************ menu bar gui
        JMenuBar menuBar = new JMenuBar();
        f.setJMenuBar(menuBar);

        // file button and submenu
        JMenu fileMenu = new JMenu("File");
        menuBar.add(fileMenu);
        JMenuItem newGame = new JMenuItem("New game");
        JMenuItem resetGame = new JMenuItem("Reset player");
        JMenuItem resignGame = new JMenuItem("Resign game");
        JMenuItem exitGame = new JMenuItem("Exit game");
        fileMenu.add(newGame);
        fileMenu.add(resetGame);
        fileMenu.add(resignGame);
        fileMenu.add(exitGame);

        // help button and submenu
        JMenu helpMenu = new JMenu("Help");
        menuBar.add(helpMenu);
        JMenuItem gameRules = new JMenuItem("How to play");
        helpMenu.add(gameRules);

        // about button and submenu
        JMenu aboutMenu = new JMenu("About");
        menuBar.add(aboutMenu);
        JMenuItem gameVersion = new JMenuItem("Game version");
        JMenuItem gameAuthor = new JMenuItem("Author");
        aboutMenu.add(gameVersion);
        aboutMenu.add(gameAuthor);

        // generating the player pawns
        JLabel p1 = new JLabel("P1");
        p1.setBounds(1120, 630, 60, 40);  // start position
        p1.setOpaque(true);
        p1.setBackground(Color.BLUE); // default colors
        JLabel p2 = new JLabel("P2");
        p2.setBounds(1120, 630, 60, 40); // start position
        p2.setOpaque(true);
        p2.setBackground(Color.YELLOW); // default colors

        gamePanel.add(p1);
        gamePanel.add(p2);

        // **************** setup before game start GUI
        // ************* layered pane for pre game player options
        JLayeredPane optionPane = new JLayeredPane();
        optionPane.setBounds(0, 0, 1600, 900);
        f.add(optionPane);

        // **** player 1 options gui
        JLabel p1name = new JLabel("Player 1 name:");
        p1name.setBounds(20, 90, 150, 40);
        optionPane.add(p1name);

        JTextField p1textField = new JTextField("");
        p1textField.setBounds(20, 120, 150, 40);
        optionPane.add(p1textField);

        JButton p1Submit = new JButton("Submit!");
        p1Submit.setBounds(20, 170, 150, 40);
        optionPane.add(p1Submit);

        JLabel p1Color = new JLabel("Player 1 color:");
        p1Color.setBounds(20, 220, 150, 40);
        optionPane.add(p1Color);

        JButton colorP1Blue = new JButton();
        colorP1Blue.setBounds(20, 270, 40, 40);
        colorP1Blue.setBackground(Color.BLUE);
        optionPane.add(colorP1Blue);

        JButton colorP1Red = new JButton();
        colorP1Red.setBounds(75, 270, 40, 40);
        colorP1Red.setBackground(Color.RED);
        optionPane.add(colorP1Red);

        // submit button for p1 name
        p1Submit.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                // if nothing being submitted
                if(p1textField.getText().equals("")) {
                    p1.setText("player 1"); // default name
                } else {
                    p1.setText(p1textField.getText()); // set given name to p1
                }
            }
        });

        // p1 color set to blue
        colorP1Blue.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                p1.setBackground(Color.BLUE);
            }
        });

        // p1 color set to red
        colorP1Red.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                p1.setBackground(Color.RED);
            }
        });

        // **** player 2 options gui
        JLabel p2name = new JLabel("Player 2 name:");
        p2name.setBounds(20, 420, 150, 40);
        optionPane.add(p2name);

        JTextField p2textField = new JTextField("");
        p2textField.setBounds(20, 450, 150, 40);
        optionPane.add(p2textField);

        JButton p2Submit = new JButton("Submit!");
        p2Submit.setBounds(20, 500, 150, 40);
        optionPane.add(p2Submit);

        JLabel p2Color = new JLabel("Player 2 color:");
        p2Color.setBounds(20, 550, 150, 40);
        optionPane.add(p2Color);

        JButton colorP2Yellow = new JButton();
        colorP2Yellow.setBounds(20, 600, 40, 40);
        colorP2Yellow.setBackground(Color.YELLOW);
        optionPane.add(colorP2Yellow);

        JButton colorP2Green = new JButton();
        colorP2Green.setBounds(75, 600, 40, 40);
        colorP2Green.setBackground(Color.GREEN);
        optionPane.add(colorP2Green);

        // submit button for p2 name
        p2Submit.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                // if nothing being submitted
                if(p2textField.getText().equals("")) {
                    p2.setText("player 2"); // default name
                } else {
                    p2.setText(p2textField.getText()); // set given name to p2
                }
            }
        });

        // p2 color set to green
        colorP2Green.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                p2.setBackground(Color.GREEN);
            }
        });

        // p2 color set to yellow
        colorP2Yellow.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                p2.setBackground(Color.yellow);
            }
        });

        // *********** board gui
        JLabel l = new JLabel("Snakes And Ladders!");
        l.setBounds(20,20,150,40);

        JLabel turnLabel = new JLabel("Player 1's turn!");
        turnLabel.setBounds(300, 20, 150, 40);

        turnLabel.setVisible(false);

        JButton startButton = new JButton("Start");
        startButton.setBounds(300, 100, 100, 40);

        JButton rollDiceButton = new JButton("Roll Dice!");
        rollDiceButton.setBounds(700, 100, 100, 40);
        rollDiceButton.setEnabled(false);

        JLabel dice1 = new JLabel("Dice 1:");
        dice1.setBounds(700, 150, 50, 40);

        JLabel dice2 = new JLabel("Dice 2:");
        dice2.setBounds(760, 150, 50, 40);

        JButton displayDice1Button = new JButton();
        displayDice1Button.setBounds(690, 200, 50, 40);

        JButton displayDice2Button = new JButton();
        displayDice2Button.setBounds(760, 200, 50, 40);

        JLabel boardRules = new JLabel("Cyan tile: ladder start - Orange tile: snake start - click on tile");
        boardRules.setBounds(900, 60, 450, 40);

        JLabel p1Pos = new JLabel("Player 1 position:");
        p1Pos.setBounds(900, 120, 100, 40);
        p1Position.setBounds(900, 200, 100, 40);

        JLabel p2Pos = new JLabel("Player 2 position:");
        p2Pos.setBounds(1200, 120, 100, 40);
        p2Position.setBounds(1200, 200, 100, 40);

        // adding elements to the layered pane
        try{
            gamePanel.add(l);
            gamePanel.add(turnLabel);
            gamePanel.add(displayDice1Button);
            gamePanel.add(displayDice2Button);
            gamePanel.add(dice1);
            gamePanel.add(dice2);
            gamePanel.add(rollDiceButton);
            gamePanel.add(startButton);
            gamePanel.add(boardRules);
            gamePanel.add(p1Pos);
            gamePanel.add(p1Position);
            gamePanel.add(p2Pos);
            gamePanel.add(p2Position);
        }
        catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Something went wrong with adding elements to the game panel");
        }


        // ************ action listeners for buttons

        // start button
        startButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                // checks if the game is started
                if(gameStarted) {
                    startButton.setEnabled(false);
                } else {

                    gameStarted = true;

                    startButton.setEnabled(false);

                    // hides the pre game options
                    optionPane.setVisible(false);


                    // enabling the roll button and the label to show whose turn it is
                    rollDiceButton.setEnabled(true);

                    turnLabel.setVisible(true);
                }
            }
        });

        // roll dice button
        rollDiceButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                int diceCount = 2; // number of dice
                int roll1 = 0;   // count for dice roll
                int roll2 = 0;
                int rolls = 0;

                optionPane.setVisible(false);

                // rolling dice
                for(int i = 0; i < diceCount; i++) {
                    // getting a random number 1-6 to simulate dice roll
                    Random r = new Random();
                    int j = r.nextInt(6) + 1;

                    //rolls = rolls + j;    // adding the dice rolls


                    if(i == 0) {
                        roll1 += j;
                    }
                    else {
                        roll2 += j;
                    }

                    rolls = roll1 + roll2;


                    // showing the result of the two dice thrown
                    displayDice1Button.setText("" + roll1);
                    displayDice2Button.setText("" + roll2);

                }


                // starting positions on the board
                int xLoc = 1120;
                int yLoc = 630;

                // check who's turn it is
                if(turnCounter % 2 == 0) {
                    // player 1

                    if(roll1 != roll2) {
                        turnCounter++;
                        turnLabel.setText(p2.getText() + "'s turn!");
                    }

                    if(p1Counter + rolls > 36) {     // if p1 goes to far
                        // if player rolls higher than spaces left to finish, turns around and goes back after hitting the finish
                        int temp = (p1Counter + rolls) - 36; // how many steps to go back
                        p1Counter -= temp;  // updating counter

                    } else if(p1Counter + rolls == 36) {
                        // if p1 finishes
                        turnLabel.setText(p1.getText() + " won!");

                        p1Counter += rolls;
                        rollDiceButton.setEnabled(false);    // disabling roll button after finished

                    } else {
                        p1Counter += rolls;  // adding to total progress
                    }
                    p1Position.setText(Integer.toString(p1Counter));

                    // calculating board position of player 1
                    xLoc -= ((p1Counter - 1) % 9) * 100;
                    if(p1Counter % 9 == 0) {
                        yLoc -= (((p1Counter - 1) / 9) * 100);
                    } else yLoc -= ((p1Counter / 9) * 100);

                    // changing board position
                    p1.setLocation(xLoc, yLoc);

                } else {
                    // player 2

                    if(roll1 != roll2) {
                        turnCounter++;
                        turnLabel.setText(p1.getText() + "'s turn!");
                    }

                    if(p2Counter + rolls > 36) { // if p2 goes to far
                        // if player rolls higher than spaces left to finish, turns around and goes back after hitting the finish
                        int temp = (p2Counter + rolls) - 36; // how many steps to go back
                        p2Counter -= temp;  // updating counter

                    } else if(p2Counter + rolls == 36) {
                        // if p2 finishes
                        turnLabel.setText(p2.getText() + " won!");
                        p2Counter += rolls;
                        rollDiceButton.setEnabled(false);   // disabling roll button after finished

                    } else {
                        p2Counter += rolls;  // adding to total progress
                    }
                    p2Position.setText(Integer.toString(p2Counter));

                    // calculating board position of player 2
                    xLoc -= ((p2Counter - 1) % 9) * 100;
                    if(p2Counter % 9 == 0) {
                        yLoc -= (((p2Counter - 1) / 9) * 100);
                    } else  yLoc -= ((p2Counter / 9) * 100);

                    // changing board position
                    p2.setLocation(xLoc, yLoc);

                }
            }
        });

        // new game menubar button
        newGame.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                // new game - resetting variables
                turnCounter = 0;
                p1Counter = 1;
                p2Counter = 1;
                turnLabel.setText("Player 1's turn!");

                optionPane.setVisible(true);
                rollDiceButton.setEnabled(true);
                displayDice1Button.setText("");
                displayDice2Button.setText("");
                p1Position.setText("");
                p2Position.setText("");

                // resetting the players on the board
                p1.setLocation(1120, 630);
                p2.setLocation(1120, 630);

            }
        });

        // reset menubar button
        resetGame.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                // reset the player who's turn it is

                if(turnCounter % 2 == 0) {
                    // player 1 reset
                    p1.setLocation(1120, 630);  // start position
                    p1Counter = 1;
                } else {
                    // player 2 reset
                    p2.setLocation(1120, 630);  // start position
                    p2Counter = 1;
                }

            }
        });

        // resign menubar button
        resignGame.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                // resign the player who's turn it is
                if(turnCounter % 2 == 0) {
                    // player 1 resign
                    turnLabel.setText(p2textField.getText() + " won!");
                } else {
                    // player 2 resign
                    turnLabel.setText(p1textField.getText() + " won!");
                }
                rollDiceButton.setEnabled(false);
            }
        });

        // exit menubar button
        exitGame.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.exit(0);
            }
        });

        // help menubar button
        gameRules.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JOptionPane.showMessageDialog(null, "Take turns rolling the dice! \n First person to reach the finish tile wins!\n " +
                        "If you land on a snake head or a ladder start click the tile!");
            }
        });

        // game version menubar
        gameVersion.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JOptionPane.showMessageDialog(null, "Game version 1.0.0");
            }
        });

        // author menubar button
        gameAuthor.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JOptionPane.showMessageDialog(null, "Author: Jesper Ulsrud");
            }
        });

    }



    // creating all the 36 board pieces
    public static void createBoard() {

        // coordinate variables
        int x = 300;
        int y = 300;

        // for loop for each line
        for(int j = 0; j < 4; j++) {
            // for loop for each button
            for(int i = 0; i < 9; i++) {
                // updating board number and adding it to the JPanel
                place --;



                JButton b = new JButton(""+place);
                b.setBounds(x, y, 100, 100);
                x += 100;
                gamePanel.add(b);

                // ladder 1 start
                if(j == 3 && i == 4) {
                    // showing ladder start on the board
                    b.setText(place+"-L1 start");
                    b.setBackground(Color.CYAN);
                    b.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e) {
                            // moving player up the ladder if they are at the start of it
                            if(p1Counter == 5) {
                                p1Counter = 21;
                                p1Position.setText(Integer.toString(p1Counter));
                            } else if(p2Counter == 5) {
                                p2Counter = 21;
                                p2Position.setText(Integer.toString(p2Counter));
                            }
                        }
                    });
                }

                // ladder 1 end
                if(j == 1 && i == 6) {
                    // showing ladder end on the board
                    b.setText(place+"-L1 end");
                }

                // ladder 2 start
                if(j == 2 && i == 2) {
                    // showing ladder start on the board
                    b.setText(place + "-L2 start");
                    b.setBackground(Color.CYAN);
                    b.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e) {
                            // moving player up the ladder if they are at the start of it
                            if(p1Counter == 16) {
                                p1Counter = 23;
                                p1Position.setText(Integer.toString(p1Counter));
                            } else if(p2Counter == 16) {
                                p2Counter = 23;
                                p2Position.setText(Integer.toString(p2Counter));
                            }
                        }
                    });



                }

                // ladder 2 end
                if(j == 1 && i == 4) {
                    // showing ladder end on the board
                    b.setText(place+"-L2 end");
                }

                // snake 1 start
                if(j == 1 && i == 7) {
                    // showing snake head on the board
                    b.setText(place + "-S1 start");
                    b.setBackground(Color.ORANGE);
                    b.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e) {
                            // moving player down the ladder if they are at the start of it
                            if(p1Counter == 20) {
                                p1Counter = 9;
                                p1Position.setText(Integer.toString(p1Counter));
                            } else if(p2Counter == 20) {
                                p2Counter = 9;
                                p2Position.setText(Integer.toString(p2Counter));
                            }
                        }
                    });
                }

                // snake 1 end
                if(j == 3 && i == 0) {
                    // showing snake end on the board
                    b.setText(place+"-S1 end");
                }

                // snake 2 start
                if(j == 0 && i == 3) {
                    // showing snake start on the board
                    b.setText(place + "-S2 start");
                    b.setBackground(Color.ORANGE);
                    b.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e) {
                            // moving player down the ladder if they are at the start of it
                            if(p1Counter == 33) {
                                p1Counter = 12;
                                p1Position.setText(Integer.toString(p1Counter));
                            } else if(p2Counter == 33) {
                                p2Counter = 12;
                                p2Position.setText(Integer.toString(p2Counter));
                            }
                        }
                    });
                }

                // snake 2 end
                if(j == 2 && i == 6) {
                    // showing snake end on the board
                    b.setText(place+"-S2 end");
                }


            }
            // updating coordinate variables
            x = 300;
            y += 100;
        }
    }
}
