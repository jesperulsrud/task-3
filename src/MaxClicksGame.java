import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Timer;
import java.util.TimerTask;

public class MaxClicksGame extends JFrame {

    // Game rules being displayed
    static String gRules = "See how many times you can do in a minute!";

    // variables used for counting and values
    int record = 0;
    int clickCounter = 0;
    int seconds = 60;

    MaxClicksGame() {


        // initiating all the GUI elements
        JLabel l = new JLabel("Maximum Clicks");
        l.setBounds(20,20,100,40);

        JLabel gameRules = new JLabel(gRules);
        gameRules.setBounds(30, 60, 340,80);

        JLabel recordValue = new JLabel("Record:  " + record + " clicks");
        recordValue.setBounds(30, 90, 340,80);

        JButton startButton = new JButton("Start");
        startButton.setBounds(65,180, 250, 40);

        JButton clickButton = new JButton("Click ME!");
        clickButton.setBounds(65,230, 120, 40);

        JTextField counterText = new JTextField("  counter");
        counterText.setBounds(195, 230, 120, 40);

        JLabel timeLabel = new JLabel("Time Left: ");
        timeLabel.setBounds(80,290,60,40);

        JTextField secondsCounter = new JTextField("   60 ");
        secondsCounter.setBounds(150, 290, 40, 40);

        JLabel secondsLabel = new JLabel("seconds");
        secondsLabel.setBounds(200,290,60,40);

        // adding the elements to the JFrame
        add(l);
        add(gameRules);
        add(recordValue);
        add(startButton);
        add(clickButton);
        add(counterText);
        add(timeLabel);
        add(secondsLabel);
        add(secondsCounter);

        // setting the size of the JFrame and making it visible
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(400, 400);
        setLayout(null);
        setVisible(true);

        // setting the click me button to unavailable
        clickButton.setEnabled(false);

        // initiate the timer and timer task
        Timer timer = new Timer();
        TimerTask task = new TimerTask() {
            @Override
            public void run() {
                // counting down from 60 and updating the text box
                seconds--;
                secondsCounter.setText("   " + seconds);

                // stopping the timer at 0
                if(seconds == 0) {
                    timer.cancel();
                    timer.purge();

                    // displaying message game finished
                    JOptionPane.showMessageDialog(null, "Game finished!");

                    // updating record if the counter is more than the current record
                    if(clickCounter > record) {
                        record = clickCounter;
                        recordValue.setText("Record:  " + record + " clicks");
                    }

                    // resetting the counter
                    clickCounter = 0;
                }
            }
        };

        // action listener for the start button
        startButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                // if game is already finished
                if(seconds == 0) {
                    JOptionPane.showMessageDialog(null, "Game finished!");
                } else {
                    // make the click button available
                    clickButton.setEnabled(true);

                    // start counting from a minute
                    timer.scheduleAtFixedRate(task, 1000, 1000);
                }
            }
        });

        // action listener for the click button
        clickButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                // if theres still time left
                if(seconds > 0) {
                    // update click counter and display current number of clicks
                    clickCounter++;
                    counterText.setText(clickCounter + " clicks");
                } else {
                    // display game finished message if the time is out
                    JOptionPane.showMessageDialog(null, "Game finished!");
                }
            }
        });



    }




}
