import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Random;

public class DiceGame extends JFrame {

    // Game rules being displayed
    static String gRules = " Each player takes three alternate turns to roll a dice. The one\n with the highest score after three turns win!";

    // variables used for counting score and turns
    int playerTotal1 = 0;
    int playerTotal2 = 0;
    int rollCnt1 = 0;
    int rollCnt2 = 0;

    DiceGame() {

        // initiating all the GUI elements
        JLabel l = new JLabel("Roll a Dice");
        l.setBounds(20,20,100,40);

        JLabel gameRules = new JLabel(gRules);
        gameRules.setBounds(30, 60, 340,80);

        JButton player1 = new JButton("Player 1");
        player1.setBounds(75,150, 100, 40);

        JButton player2 = new JButton("Player 2");
        player2.setBounds(200, 150, 100, 40);

        JTextField diceValue = new JTextField("dice value");
        diceValue.setBounds(135, 220, 100,40);

        JTextField pScore1 = new JTextField("player 1 score");
        pScore1.setBounds(75, 285, 100,40);

        JTextField pScore2 = new JTextField("player 2 score");
        pScore2.setBounds(200, 285, 100,40);

        // adding the elements to the JFrame
        add(l);
        add(gameRules);
        add(player1);
        add(player2);
        add(diceValue);
        add(pScore1);
        add(pScore2);

        // setting the size of the JFrame and making it visible
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(400, 400);
        setLayout(null);
        setVisible(true);


        // listener for what happens when you push the player1 button
        player1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                // checking if the game is finished
                if(rollCnt2 == 3) {
                    JOptionPane.showMessageDialog(null, "Game is finished!");
                } else {
                    // checking if its the right players turn
                    if(rollCnt1 == rollCnt2) {
                        // getting a random number from 1-6 to simulate a dice roll
                        Random rand = new Random();
                        int i = rand.nextInt(5);
                        int temp = i + 1;

                        // displaying the dice value
                        diceValue.setText(Integer.toString(temp));

                        // counting the total score for player 1
                        playerTotal1 = playerTotal1 + temp;

                        // displaying the total score for player 1
                        pScore1.setText(Integer.toString(playerTotal1));

                        // adding to the roll counter
                        rollCnt1++;
                    } else {
                        JOptionPane.showMessageDialog(null, "Not your turn!");
                    }
                }
            }
        });

        // listener for what happens when you push the player2 button
        player2.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                // checking if the game is finished
                if(rollCnt2 == 3) {
                    JOptionPane.showMessageDialog(null, "Game is finished!");
                } else {
                    // checking if its the right players turn
                    if(rollCnt1 == rollCnt2 + 1) {

                        // getting a random number from 1-6 to simulate a dice roll
                        Random rand = new Random();
                        int i = rand.nextInt(5);
                        int temp = i + 1;

                        // displaying the dice value
                        diceValue.setText(Integer.toString(temp));

                        // counting the total score for player 2
                        playerTotal2 = playerTotal2 + temp;

                        // displaying the total score for player 2
                        pScore2.setText(Integer.toString(playerTotal2));

                        // adding to the roll counter
                        rollCnt2++;

                        // Checking the game result if each player have rolled 3 times
                        if(rollCnt2 == 3) {
                            // if tied
                            if(playerTotal1 == playerTotal2) {
                                JOptionPane.showMessageDialog(null, "Tied!");
                            // if player 1 won
                            } else if(playerTotal1 > playerTotal2) {
                                JOptionPane.showMessageDialog(null, "Player 1 won!");
                            // if player 2 won
                            } else {
                                JOptionPane.showMessageDialog(null, "Player 2 won!");
                            }
                        }
                    } else {
                        JOptionPane.showMessageDialog(null, "Not your turn!");
                    }
                }
            }
        });
    }
}
