import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Main {

    public static void main(String[] args) {

        JFrame f = new JFrame();


        JLabel l = new JLabel("Game Center");
        l.setBounds(20,10, 100, 40);

        JButton diceButton = new JButton("Roll a Dice");
        diceButton.setBounds(75,100,150,40);

        JButton maxButton = new JButton("Maximum Clicks");
        maxButton.setBounds(75,175,150,40);

        JButton snakesAndLaddersButton = new JButton("Snakes And Ladders");
        snakesAndLaddersButton.setBounds(75, 250, 150, 40);

        JButton exitButton = new JButton("Exit");
        exitButton.setBounds(255,250, 75,40);

        f.add(l);
        f.add(diceButton);
        f.add(maxButton);
        f.add(snakesAndLaddersButton);
        f.add(exitButton);

        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        f.setSize(400, 400);
        f. setLayout(null);
        f.setVisible(true);

        diceButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                new DiceGame();
            }
        });

        maxButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                new MaxClicksGame();
            }
        });

        snakesAndLaddersButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                new SnakesAndLaddersGame();
            }
        });

        exitButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.exit(0);
            }
        });


    }
}
